# Front End Test

## FAQ

### 1. Bootstrap / LESS

**1.1 Q: What are your thoughts when considering a frontend framework like Bootstrap for a project?**

A: My opinion about the bootstrap is quite respectful. I believe that this is an excellent tool for prototyping.
Many startups have chosen a bootstrap for the rapid development of their product.
I like Bootstrap because it customizable, consistent UI, responsive structures, js-plugins using the jQuery, good documentation,
good grid system, icons included.
But I think that Bootstrap has a number of disadvantages, because of this I prefer other tools for building a layout.
The Disadvantages of Bootstrap are:
1. A large number of style overrides
2. Limitations in the design due to the complexity of customization
3. Excessive html-structure complexity, which affects semantics

I prefer to use Flexbox and Grid Layout, because I have clean code and more mobility in the structure.

**1.3 Q: When does it (normally) make sense to use CSS preprocessor like LESS/SASS and what are the biggest benefits you see?**

A: I use it in all projects. Main reasons for that:
1. I can use variables. I can use in in modern CSS too, but this is not suitable for all projects due to cross-browser compatibility.
2. I can use mixins for faster development and refusal of code duplication.
3. I can use nesting for clean and readable code.
4. I can use modular structure.

### 2. jQuery / JS

**2.0 Q: How do you structure / encapsulate your JS code if your NOT building on top of a JS lib/framework like AngularJS, ReactJS etc.?**

A: For small projects I can use simple scripts in single file or include scripts to pages.
For big projects I can write classes with methods, but the better way - use some framework.
If I'll write a large number of classes, sooner or later it will turn into my own framework, without normal documentation and community.
It's good for my skills, but not very good for the project, because a new member of the team will find it hard to start working with this framework.

**2.1 Q: When does it make sense to use a jQuery plugin script vs. implementing a functionality yourself?**

A: I would better use jQuery plugin under the following conditions:
1. The functionality of the plugin meets the requirement
2. The speed of integration is much less than developing my own solution
3. The plugin has a GitHub-repository and was updated at least 2 years ago
4. I'll read the main issues in the repository so that I don't accidentally use the plug-in that will lead to problems
5. The plugin has high-quality code and does not affect other project scripts

### 3. Ajax / HTTP

**3.2 Q: When does it make sense to submit a form as a non-AJAX (regular) request?**

A: This approach should be applied when the action is considered complete:
- Contact form
- Quiz and test
- Shopping carts

This is not suitable for:
- Comments
- Online-messengers
- Profiles

And, of course, all forms must work when JS is disabled in browser (Graceful degradation).

**3.3 Q: What relevance does it have for the frontend JS-code that the server endpoints respond with different HTTP codes? Which HTTP codes do you consider most important?**

A: Most important codes for frontend:
- 200 (OK)
- 400 (Bad Request)
- 401 (Unauthorized), 403 (Forbidden)
- 404 (Not Found)
- 500 (Internal Server Error)

I can use the codes for debugging and logs.