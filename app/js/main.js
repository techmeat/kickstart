$(function() {
  var groupClass = '.form-group';
  var buttonElem = $('#submitButton');
  var buttonText = buttonElem.text();
  var messageState = true;

  // Validation of person form
  $('#personForm').validate({
    errorClass: 'has-error',
    validClass: 'has-success',
    rules: {
      'firstName': {
        required: true,
        rangelength: [3, 50]
      },
      'lastName': {
        required: true,
        rangelength: [3, 50]
      },
      'gender': {
        required: true
      }
    },
    messages: {
      'firstName': {
        required: 'Please enter your first name',
        rangelength: 'Please enter at least {0} characters'
      },
      'lastName': {
        required: 'Please enter your last name',
        rangelength: 'Please enter at least {0} characters'
      },
      'gender': {
        required: 'Please select your gender'
      }
    },
    errorElement: 'em',
    errorPlacement: function (error, element) {
      error.addClass('help-block');
      error.insertAfter(element);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).closest(groupClass).addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).closest(groupClass).addClass(validClass).removeClass(errorClass);
    },
    showErrors: function() {
      this.defaultShowErrors();

      if (!this.numberOfInvalids()) {
        showMessage(false);
      } else {
        showMessage(true, 'error', 'Your form contains ' + this.numberOfInvalids() + ' errors, see details below.');
      }
    },
    invalidHandler: function(form, validator) {
      var errorsQuantity = validator.numberOfInvalids();
      if (errorsQuantity) {
        showMessage(true, 'error', 'Your form contains ' + errorsQuantity + ' errors, see details below.');
      }
      validator.focusInvalid();

      showErrorsPopup(errorsQuantity, validator.submitted);
    },
    submitHandler: function(form) {
      messageState = true;
      disableSubmitButton(true);

      $.ajax({
        type: 'POST',
        url: 'http://pro.techmeat.net/2018/08/kickstart/profile.php',
        data: $(form).serialize(),
        success: function () {
          showMessage(true, 'success', 'Submitted successfully');
        },
        error: function(){
          showMessage(true, 'error', 'There is error while submit');
        }
      });

      setTimeout(function () {
        disableSubmitButton(false);
      }, 1000);

      return false;
    }
  });

  // Activate or deactivate message block
  function showMessage(state, type, text) {
    var messageElem = $('#summary');

    if (state && messageState) {
      var messageOldClass = 'alert-info';
      var messageNewClass = 'alert-info';

      if (type === 'error') {
        messageOldClass = 'alert-success';
        messageNewClass = 'alert-danger';
      } else if (type === 'success') {
        messageOldClass = 'alert-danger';
        messageNewClass = 'alert-success';
      }

      messageElem.slideDown().removeClass(messageOldClass).addClass(messageNewClass).find('.alert-text').html(text);
    } else {
      messageElem.slideUp();
    }
  }

  // Compile error list for message box
  function completeErrorsList(number, messages) {
    var errorListContent = '<ol>';

    for (var i = 0; i < number; i++) {
      errorListContent = errorListContent + '<li>' + Object.values(messages)[i] + '</li>';
    }

    return errorListContent + '</ol>';
  }

  // Activate popup with list of errors
  function showErrorsPopup(number, messages) {
    setTimeout(function () {
      $.colorbox({
        html: '<div class="panel panel-default">' +
        '<div class="panel-heading"><h3 class="panel-title">Your form contains '
        + number + ' errors</h3></div>' +
        '<div class="panel-body">' + completeErrorsList(number, messages) + '</div>',
        initialWidth: '0',
        initialHeight: '0'
      });
    });
  }

  // Disable button when form is submitting
  function disableSubmitButton (status) {
    if (status) {
      buttonElem.attr('disabled', true).html('<span class="glyphicon glyphicon-refresh main__refresh"></span>');
    } else {
      buttonElem.attr('disabled', false).text(buttonText);
    }
  }

  // Close a message box
  $('.js-close-message').on('click', function () {
    messageState = false;
    showMessage(false);
  });
});